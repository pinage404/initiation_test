---
selectable: true
canvasWidth: 700
lineNumbers: true
defaults:
  layout: center
  class: text-center
---


# Tests logiciels

---

## Introduction

---

### ⚠️

#### 🦸🏻‍♂️🧑🏻‍🦼❌

<!--
je ne suis pas Charles Xavier / Professor X : je ne suis pas télépathe

donc si vous ne dites pas que vous ne comprennez pas,
je ne peux pas le savoir

il n'y a aucune questions stupides

si vous ne dites pas que vous ne savez pas, vous n'apprendrez pas

n'hésitez pas à demander tant que vous n'avez pas compris
-->

---

### ⚠️

<v-clicks>

* 🪛<v-click at="5">🚫➡️🛤️</v-click>
* 🔨<v-click at="5">🚫➡️🛤️</v-click>
* 🔧<v-click at="5">🚫➡️🛤️</v-click>
* 🧰<v-click at="5">🚫➡️🛤️</v-click>

</v-clicks>

<!--
langage, framework, lib ... ➡️ outils

méthode

carrière
-->

---

### ⚠️

Jouez le jeu

<!--
pendant les exercices,
vous allez faire semblant que vous croyez que les méthodes proposées sont une bonne chose

vous déciderez ensuite si vous voulez les utiliser ou non
-->

---

* Tour de table
  * votre [niveau de connaissance](./pages/niveaux.md)
    * honnete
    * sans jugement
  * vos attentes

---
src: ./pages/niveaux.md
---

## Pratique

---

### 🥋

[Kata](https://en.wikipedia.org/wiki/Kata#Outside_martial_arts)

[Dave Thomas 2003](https://web.archive.org/web/20040607052051/http://pragprog.com/pragdave/Practices/Kata)

<!--
un bon moyen de s'entrainer à faire des tests, c'est de faire des katas
-->

---

### 🏯

[Dojo](https://en.wikipedia.org/wiki/Dojo#Computer-related)

[Laurent Bossavit 2004](https://web.archive.org/web/20040605124346/http://bossavit.com/thoughts/)

<v-click>

[Dojo développement Paris 📜](https://github.com/dojo-developpement-paris/dojo-developpement-paris.github.io/blob/main/history.md)

</v-click>

<!--
> Si je veux apprendre le Judo, je vais m'inscrire au dojo du coin et y passer une heure par semaine pendant deux ans, au bout de quoi j'aurai peut-etre envie de pratiquer plus assidument.
>
> Des années d'entraînement supplémentaire peuvent être récompensées par l'obtention d'une ceinture noire, qui n'est que le signe d'une ascension vers une autre étape de l'apprentissage. Aucun maître ne cesse d'apprendre.
>
> Si je veux apprendre la programmation objet, mon employeur va me trouver une formation de trois jours à Java dans le catalogue d'une société de formation.
>
> Qu'est-ce qui ne va pas avec cette photo ?
>
> Dave Thomas a eu la bonne idée avec son Kata, mais il faut aller plus loin.
>
> Ce que je veux, c'est un dojo de programmation objet.
-->

---

### 🧪

<v-clicks>

* ❌💣💥
* ✅⚙️🧠💡

</v-clicks>

---

### 👥

---
layout: quote
---

### 🗣️🧑‍💻️

Strong Style

> For an idea to go from your head into the computer
> it MUST go through someone else's hands
>
> [Llewellyn Falco 2014](https://llewellynfalco.blogspot.com/2014/06/llewellyns-strong-style-pairing.html)

---

## Théorie

---

### Pourquoi on test ?

---
layout: iframe-left
url: https://lesjoiesducode.fr/captain-america-marche-sur-mon-poste-meme-developpeur
---

### Pourquoi on test ?

---

### Pourquoi on test ?

Sérénité 😌

---

### Différent types de tests

<v-clicks depth="2">

* Manuel 👤
  * Exploratoire
  * ...
* Automatisés 🤖
  * Test Unitaire (_TU_)
  * Test d'Intégration (_TI_)
  * Test End to End (_E2E_)
  * ...

</v-clicks>

---

#### Pyramide des tests

---
layout: image
image: https://blog.octo.com/la-pyramide-des-tests-par-la-pratique-1-5/pyramide-1.webp
backgroundSize: contain, contain
style: |
  background-image:
    url("https://blog.octo.com/la-pyramide-des-tests-par-la-pratique-1-5/pyramide-1.webp"),
    linear-gradient(to right bottom, hsl(0, 0%, 40%), hsl(0, 0%, 90%))
---

<small>

[_La pyramide des tests par la pratique_ par Jérôme Van Der Linden chez Octo](https://blog.octo.com/la-pyramide-des-tests-par-la-pratique-1-5/)

</small>

<!--
tendre vers cet idéal pour :
* **maximiser la confiance**
* **minimiser les couts**
* minimiser les temps d'attentes
* minimiser les temps de maintenance
-->

---

#### Test E2E

<v-click>

🤖🖱️⌨️💻🛜🖥️

</v-click>

---

#### Test d'Intégration

😀💬 🗨️🙂

<v-clicks>

😣🗯️ 🗮😕

😕 🗱 😮‍💨

</v-clicks>

---

#### Test Unitaire

Vérifie le comportement d'une unité fonctionnelle

---

### Tests automatisés 🤖

---
layout: iframe-right
url: https://www.commitstrip.com/fr/2013/10/11/mieux-et-moins-cher-que-les-tests-unitaires/
---

---
layout: image-left
image: /xp_feedback_loops.png
backgroundSize: contain
---

#### Réduire la boucle de feedback ➰

[eXtreme Programming feedback loops](http://www.extremeprogramming.org/map/loops.html)

---

#### Vérification

---

#### Avantages

<v-clicks>

* **sérénité** 😌
* **confiance**
* refactoring
* non régression
* maintenabilité
* documentation
* design
* ...

</v-clicks>

<!--
design logiciel pas UI +/- synonyme architecture
-->

---

#### Inconvénients

<v-clicks depth="2">

* apprentissage
  * ralentit
* trouver les bons tests
* contrainte méthodologique
  * ré-apprentissage de la manière de dev

</v-clicks>

<!--
vous developpiez moins rapidement lorsque vous appreniez à dev
-->

---

**Contraintes** 😒 🆚 **Garanties** 😌

🎭 🪙

<!--
2 faces d'une meme pièce
-->

---

Contrainte : s'arreter au feu rouge 🚶🚦🚗

<v-click>

Garantie : éviter un accident au carrefour 🚗💨😵🚦

</v-click>

---

Contrainte : cotiser pour le chomage 💸

<v-clicks>

Garantie `*` : éviter de se retrouver à la rue si on perd son taf 👥💰😌

`*` _si on oublie les néo-libéraux_

</v-clicks>

---

Contrainte : méthodes pour écrire les tests 🤔

<v-click>

Garantie : tests de confiance 😌

</v-click>

<!--
méthode longue à apprendre
-->

---
layout: default
---

#### F.I.R.S.T. Principles 1️⃣

* **F**
* **I**
* **R**
* **S**
* **T**

<!--
faire deviner les principes
-->

---
layout: default
---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
  * < secondes
  * lent ➞ flemme ➞ non-exécuté ➞ ignoré
* **I**
* **R**
* **S**
* **T**

---
layout: default
---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
  * pas de dépendances entre les tests
  * pas d'ordre d'exécution des tests
  * évite les problèmes en cascade
* **R**
* **S**
* **T**

---
layout: default
---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
* **R**epeatable
  * "erreur normale" / "test flaky" ➞ ignoré
    * faux négatif / vrai négatif ?
    * faux positif 😰
* **S**
* **T**

---
layout: default
---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
* **R**epeatable
* **S**elf-validating
  * `assert`ion dans chaque test
  * pas de vérification manuelle dans l'output / un fichier
    * fastidieux ➞ lent ➞ flemme ➞ non-exécuté ➞ ignoré
    * subjectif ➞ désaccord sur le résultat
* **T**

---
layout: default
---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
* **R**epeatable
* **S**elf-validating
* **T**imely
  * _code de **test**_ écrit **avant** _code de **prod**_

---
layout: default
---

#### F.I.R.S.T. Principles 1️⃣

* **F**ast
* **I**solated
* **R**epeatable
* **S**elf-validating
* **T**imely

[_Writing your F.I.R.S.T. Unit Tests_ par Lucas Fonseca Mundim](https://dev.to/mundim/writing-your-f-i-r-s-t-unit-tests-1iop)

---

#### TDD

<v-click>

Test Driven Development

[TDD Manifesto](https://tddmanifesto.com/)

</v-click>

<!--
Faire deviner le nom

**Dévelopement dirigé par les tests**

Faire deviner les 3 phases
-->

---
src: ./pages/graph/all_in_one.md
zoom: 0.95
---

---
src: ./pages/graph/red.md
zoom: 0.75
---

---
src: ./pages/graph/green.md
zoom: 0.70
---

---
src: ./pages/graph/refactor.md
zoom: 1.3
---

---
zoom: 1.2
---

Ajout de **valeur incrémentale** 📈➰

---

Baby steps 👶🦶

---

#### TPP

<v-click>

[Transformation Priority Premise](https://en.wikipedia.org/wiki/Transformation_Priority_Premise)

</v-click>

<v-clicks>

1. Fake
2. Triangulation
3. Généralisation

</v-clicks>

---

Axes de compléxité

<!--
♟️
-->

---

Documentation as Code

<v-click>

Living documentation

[Cyrille Martraire 2019 📖](https://informit.com/livingdoc)

</v-click>

---

Cas d'exemple réel

<!--
ou au moins réalistes
-->

---

Design émergeant

<v-click>

J'ai besoin d'un moyen de locomotion :

</v-click>

<v-clicks>

* 🛹
* 🛴
* 🚲
* 🛵
* 🛺

</v-clicks>

<!--
lacher prise

j'ai besoin de pouvoir m'orienter facilement

~~je suis fainéant~~

je veux pouvoir etre assis

je ne veux pas faire d'effort

je ne veux pas conduire moi meme
-->

---

⬛ Black box

🆚

⬜ White box

---

Se déplacer sur rails 🛤️

|             |       |       |
| :---------- | :---: | :---: |
| ⬛ Black box |   🚂   |   🚆   |
| ⬜ White box |   🌫️   |   ⚡   |

---

XXième siècle

Je veux avoir une calèche qui roule à 40km/h

---

Je veux avoir **une calèche** qui roule à 40km/h

---

Je veux avoir **une calèche** qui roule à 40km/h 🐴

Je veux pouvoir **me déplacer** à 40km/h 🚗

---

Tester les détails d'implémentation

<v-clicks>

➡️ Implémentation figée ❌

Comportement ✅

</v-clicks>

---

Test Double / Doublure de test

<v-click>

* Fake
* Dummy
* Stub
* Spy
* Mock

[Martin Fowler 2006](https://martinfowler.com/bliki/TestDouble.html)

</v-click>

<!--
* Fake : implem réelle mais inutilisable en prod
* Dummy : remplit les arguments mais pas utilisé
* Stub : fourni des data
* Spy : stub qui enregistre comment on interagit avec
* Mock : échoue s'il n'est pas appelé de la manière attendue
-->

---

#### Code Legacy

<v-clicks>

* écrit par qqn d'autre
* *buggué*
* "vieux"
* "crade"
* code sans test
* ...

</v-clicks>

---

#### Code Legacy

<v-clicks>

1. retro test
1. refacto
1. feature

</v-clicks>

<!--
définition legacy
-->

---

> for each desired change,
> make the change easy **(warning: this may be hard)**,
> then make the easy change

[Kent Beck 2012](https://twitter.com/KentBeck/status/250733358307500032)

---

Test Paramétré

<v-clicks>

> L'abus de tests paramétrés est dangereux pour la maintenabilité
>
> À utiliser avec modération

Une règle = un test (ou plus)

`expect` paramétré = Code Smell 👃

</v-clicks>

---

Condition dans un test

<v-click>

Plusieurs règles vérifiées d'un coup 👃

</v-click>

---
layout: two-cols-header
---

#### Structure d'un test

::left::

<v-clicks>

1. Arrange
1. Act
1. Assert

</v-clicks>

::right::

<v-clicks>

1. Given
2. When
3. Then

</v-clicks>

---

#### SUT

<v-click>System Under Test</v-click>

---

Outside-in ⬇️

🆚

Inside-out ⬆️

---

Mockist

🆚

Classicist

---

#### ATDD

<v-clicks>

Acceptance Test Driven Development

AKA Double Loop TDD

</v-clicks>

---

#### PBT

<v-click>Property Based Testing</v-click>

---

#### Mutation Testing

---

#### Golden Master

<v-click>Snapshot testing</v-click>

---

#### TCR

<v-clicks>

`test && commit || revert`

Baby steps 👶🦶

</v-clicks>

---

##### TCRDD

<v-clicks>

🟢 green

TCR + TDD

<div>

`test && revert || commit` 🔴 red

`test && commit || revert` 🟢 green

[Xavier Detant 2018](https://blog.zenika.com/2018/12/03/tdd-est-mort-longue-vie-tcr/)

</div>

</v-clicks>

<!--
Xavier's [tcrdd](https://github.com/FaustXVI/tcrdd)

[talk](https://youtu.be/oldZGW9MH2s?si=3xSKA3ijfBToV7ur)
-->

---

IA 🤖

<v-click>

🤷😕

</v-click>

<!--
je n'ai pas testé

mes amis qui ont testé m'ont dit que
c'est un dev très junior (et un peu con) qui n'a aucune méthodologie
-->

---

✏️📼

<v-clicks>

💥☀️🌋☄️💥🌔🌋⛰️💧🦠🌲🦕🦖☄️🦣🐒🧑👋

👶 ➡️ 🧮🧑‍⚕️🎨🗿🏺📜

👴🏻 ➡️ 🗓️⏳💀

📈➿

</v-clicks>

---

## Points à retenir

<v-clicks>

* Accélérer la feedback loop ➰
* FIRST Principles 1️⃣
* TDD 🔴🟢🔵
* Baby steps 👶🦶

</v-clicks>

---
src: ./pages/questions.html
---
